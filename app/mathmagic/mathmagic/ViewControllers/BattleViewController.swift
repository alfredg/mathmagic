//
//  BattleViewController.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/15.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import UIKit
import YLProgressBar

class BattleViewController: UIViewController {
    let app = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var buttonSkip: UIButton!
    
    @IBOutlet weak var assistent1BackgroundImage: UIImageView!
    @IBOutlet weak var assistent1Image: UIImageView!
    @IBOutlet weak var assistent2BackgroundImage: UIImageView!
    @IBOutlet weak var assistent2Image: UIImageView!
    
    @IBOutlet weak var enemyHeroImage: UIImageView!
    @IBOutlet weak var enemyAssistent1Image: UIImageView!
    @IBOutlet weak var enemyAssistent2Image: UIImageView!
    
    @IBOutlet weak var enemyStatusBackground: UIImageView!
    @IBOutlet weak var enemyName: UILabel!
    @IBOutlet weak var enemyLevel: UILabel!
    @IBOutlet weak var enemyHPBar: YLProgressBar!
    @IBOutlet weak var enemyMPBar: YLProgressBar!
    
    
    @IBOutlet weak var statusBackground: UIImageView!
    @IBOutlet weak var statusName: UILabel!
    @IBOutlet weak var statusLevel: UILabel!
    @IBOutlet weak var statusHPBar: YLProgressBar!
    @IBOutlet weak var statusMPBar: YLProgressBar!
    
    
    @IBOutlet weak var buttonSubmit: UIButton!
    
    @IBOutlet weak var buttonAction1: UIButton!
    @IBOutlet weak var buttonAction2: UIButton!
    @IBOutlet weak var buttonAction3: UIButton!
    @IBOutlet weak var buttonAction4: UIButton!
    @IBOutlet weak var buttonAction5: UIButton!
    @IBOutlet weak var buttonAction6: UIButton!
    @IBOutlet weak var buttonAction7: UIButton!
    @IBOutlet weak var buttonAction8: UIButton!
    @IBOutlet weak var buttonAction9: UIButton!
    var actionButtons: [UIButton] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        log.info("\(self.app.battle.team1.mainHero.ID) 对战 \(self.app.battle.team2.mainHero.ID)")
        // themes
        backgroundImage.setImage(from: app.themeResource, key: "battle/background.image")
        if let envImage = app.levelResource.getImage("envs/" + app.battle.envKey + "/background.image") {
            backgroundImage.image = envImage
        }
        buttonSkip.loadTheme(from: app.themeResource, key: "battle/skip.button")
        
        // load characters
        assistent1BackgroundImage.setImage(from: app.themeResource, key: "battle/herobackground.image")
        assistent1Image.image = app.battle.team1.assistent1.icon
        assistent2BackgroundImage.setImage(from: app.themeResource, key: "battle/herobackground.image")
        assistent2Image.image = app.battle.team1.assistent2.icon
        
        enemyHeroImage.image = app.battle.team2.mainHero.icon
        enemyAssistent1Image.image = app.battle.team2.assistent1.icon
        enemyAssistent2Image.image = app.battle.team2.assistent2.icon
        
        // load status theme
        statusBackground.setImage(from: app.themeResource, key: "battle/statusbackground.image")
        enemyStatusBackground.setImage(from: app.themeResource, key: "battle/enemystatusbackground.image")
        // set status
        enemyName.text = app.battle.team2.name
        enemyLevel.text = "\(app.battle.team2.mainHero.level)"
        statusName.text = app.battle.team1.name
        statusLevel.text = "\(app.battle.team1.mainHero.level)"
        
        // 动作按钮
        actionButtons = []
        actionButtons.append(buttonAction1)
        actionButtons.append(buttonAction2)
        actionButtons.append(buttonAction3)
        actionButtons.append(buttonAction4)
        actionButtons.append(buttonAction5)
        actionButtons.append(buttonAction6)
        actionButtons.append(buttonAction7)
        actionButtons.append(buttonAction8)
        actionButtons.append(buttonAction9)
        
        for btn in actionButtons {
            btn.loadTheme(from: app.themeResource, key: "battle/action.button")
            btn.titleLabel?.adjustsFontSizeToFitWidth = true
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func skipBattleRunAway(_ sender: Any) {
        dismiss(animated: true)
    }
}
