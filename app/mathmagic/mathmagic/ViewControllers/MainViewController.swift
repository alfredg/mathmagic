//
//  MainViewController.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/10.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    let app = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var coinbar: UIView!
    @IBOutlet weak var coinbarBackgroundImage: UIImageView!
    @IBOutlet weak var coinbarButtonPlus: UIButton!
    @IBOutlet weak var coinbarCoinNumber: UILabel!
    
    @IBOutlet weak var imageLeft: UIImageView!
    @IBOutlet weak var imageRight: UIImageView!
    
    @IBOutlet weak var btnSettings: UIButton!
    @IBOutlet weak var btnStore: UIButton!
    
    @IBOutlet weak var btnStart: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // loading theme
        backgroundImage.setImage(from: app.themeResource, key: "main/background.image")
        // coinbar
        coinbarBackgroundImage.setImage(from: app.themeResource, key: "main/coinbar.bgimg")
        coinbarButtonPlus.loadTheme(from: app.themeResource, key: "main/coinbar.button")
        // buttons
        btnSettings.loadTheme(from: app.themeResource, key: "main/settings.button")
        btnStore.loadTheme(from: app.themeResource, key: "main/store.button")
        btnStart.loadTheme(from: app.themeResource, key: "main/start.button")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // update numbers
        coinbarCoinNumber.text = "\(Progress.default.coin)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        switch segue.identifier ?? "" {
        case "performtest":
            app.battle = Battle(Team(team: "teamrabbit"), fightWith: Team(team: "teamrabbit"))
        default:
            break
        }
    }


    // MARK : 
    
    @IBAction func coinbarAddCoin(_ sender: Any) {
        Progress.default.coin += 88888
        coinbarCoinNumber.text = "\(Progress.default.coin)"
    }
    
}
