//
//  StoreViewController.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/15.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import UIKit

class StoreViewController: UIViewController {
    let app = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var assistentDialogBackground: UIImageView!
    @IBOutlet weak var assistentDialog: UITextView!
    @IBOutlet weak var assistentImage: UIImageView!
    
    @IBOutlet weak var coinbarBackgroundImage: UIImageView!
    @IBOutlet weak var coinbarCoinNumber: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backgroundImage.setImage(from: app.themeResource, key: "store/background.image")
        btnBack.loadTheme(from: app.themeResource, key: "store/back.button")
        coinbarBackgroundImage.setImage(from: app.themeResource, key: "store/coinbar.bgimg")
    }
    
    private var runAnimation = true
    private func rotateBackground() {
        UIView.animate(withDuration: 15, delay: 0, options: [.curveLinear], animations: { self.backgroundImage.transform = self.backgroundImage.transform.rotated(by: CGFloat(Double.pi)) }) { _ in
            if self.runAnimation {
                self.rotateBackground()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        assistentImage.setImage(from: app.themeResource, key: "store/assistent.image1")
        assistentDialogBackground.setImage(from: app.themeResource, key: "store/assistentdialog.image")
        assistentDialog.attributedText = app.themeResource.getFormatedString("store/assistentdialog.text")
        
        rotateBackground()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        runAnimation = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissView(_ sender: Any) {
        dismiss(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
