//
//  SettingsViewController.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/14.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    let app = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var assistentImage: UIImageView!
    @IBOutlet weak var assistentDialogBackground: UIImageView!
    @IBOutlet weak var assistentDialog: UITextView!

    @IBOutlet weak var bgmVolumeLabel: UILabel!
    @IBOutlet weak var bgmVolume: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backgroundImage.setImage(from: app.themeResource, key: "settings/background.image")
        btnBack.loadTheme(from: app.themeResource, key: "settings/back.button")
        
        bgmVolumeLabel.setText(from: app.themeResource, key: "settings/bgmvolume.title")
        bgmVolume.value = Config.default.bgmVolume
    }
    
    override func viewWillAppear(_ animated: Bool) {
        assistentImage.setImage(from: app.themeResource, key: "settings/assistent.image1")
        assistentDialogBackground.setImage(from: app.themeResource, key: "settings/assistentdialog.image")
        assistentDialog.attributedText = app.themeResource.getFormatedString("settings/assistentdialog.text")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissView(_ sender: Any) {
        Config.default.save()
        dismiss(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func taptap(_ sender: Any) {
        assistentDialog.attributedText = app.themeResource.getFormatedString("settings/assistentdialog.text")
        assistentImage.setImage(from: app.themeResource, key: "settings/assistent.image1")
    }

    @IBAction func bgmVolumeChangeBegin(_ sender: Any) {
        assistentDialog.attributedText = app.themeResource.getFormatedString("settings/bgmvolumechangebegin.text")
        assistentImage.setImage(from: app.themeResource, key: "settings/assistent.image2")
    }
    
    @IBAction func bgmVolumeChanged(_ sender: Any) {
        if Config.default.bgmVolume == bgmVolume.value {
            assistentDialog.attributedText = app.themeResource.getFormatedString("settings/assistentdialog.text")
            assistentImage.setImage(from: app.themeResource, key: "settings/assistent.image1")
        } else {
            Config.default.bgmVolume = bgmVolume.value
            if let notifyTextTemplate = app.themeResource.getString("settings/bgmvolumechanged.text") {
                let notifyText = notifyTextTemplate.replacingOccurrences(of: "$newVolume", with: "\(Int(100 * Config.default.bgmVolume))")
                assistentDialog.attributedText = app.themeResource.themeTextFormater.format(notifyText)
            }
            assistentImage.setImage(from: app.themeResource, key: "settings/assistent.image3")
        }
    }
}
