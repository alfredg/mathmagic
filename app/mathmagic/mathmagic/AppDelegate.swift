//
//  AppDelegate.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/4.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import XCGLogger
import ResourcePackage
import TextFormater
import SimpleEncrypter
import MonkeyKing

/// logger
let log = XCGLogger.default

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // MARK: - 全局变量
    /// 压缩
    let _compress: SimpleEncrypter = EncrypterCompress(with: "gzip")
    
    /// 加密
    let _encrypt: SimpleEncrypter = EncrypterXor(with: "mathematica")
    
    /// 关卡资源读取器
    let levelResource = ResourcePackageReader(withCache: false,
                                              useTwoStepLocating: false,
                                              autoDeviceCustomization: false,
                                              useKeyPrefix: false)
    
    /// 界面资源读取器
    let themeResource = ResourcePackageReader(withCache: true,
                                              useTwoStepLocating: true,
                                              autoDeviceCustomization: true,
                                              useKeyPrefix: true)
    
    /// 战斗记录对象
    var battle = Battle(Team(team: "teamrabbit"), fightWith: Team(team: "teamrabbit"))
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        #if DEBUG
            log.setup(level: .debug, showThreadName: true, showLevel: true, showFileNames: true, showLineNumbers: true)
        #else
            log.setup(level: .debug, showThreadName: false, showLevel: true, showFileNames: false, showLineNumbers: false)
            let systemDestination = AppleSystemLogDestination(identifier: "mathmagic.log")
            systemDestination.outputLevel = .debug
            systemDestination.showLogIdentifier = false
            systemDestination.showFunctionName = true
            systemDestination.showThreadName = false
            systemDestination.showLevel = false
            systemDestination.showFileName = true
            systemDestination.showLineNumber = true
            systemDestination.showDate = true
            log.add(destination: systemDestination)
        #endif
        
        // 加载关卡包与主题包
        loadPackages()
        // 加载 pods 模块
        IQKeyboardManager.sharedManager().enable = true
        MonkeyKing.registerAccount(.weChat(appID: weChatAppID, appKey: weChatAppKey))
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    /// 加载资源包
    func loadPackages() {
        let filemanager = FileManager.default
        
        levelResource._logger = {s in print("Level Package: " + s)}
        //themeResource._logger = {s in print("Theme Package: " + s)}
        
        // 清除临时文件
        log.debug("清除临时文件")
        do {
            let tmppath = NSTemporaryDirectory()
            log.debug("  检索临时文件目录 [\(tmppath)]")
            let tmpfiles = try filemanager.contentsOfDirectory(atPath: tmppath)
            log.debug("  临时文件共[\(tmpfiles.count)]个, 准备删除")
            for file in tmpfiles {
                log.debug("    删除文件 [\(file)]")
                try filemanager.removeItem(atPath: tmppath + file)
            }
        } catch {
            log.error("临时文件删除失败")
        }
        
        // loading defautl packages
        levelResource.packages["default"] = ResourcePackage(with: Bundle.main.path(forResource: "levels", ofType: "pkg")!, encrypter: _encrypt, compressor: _compress)
        themeResource.packages["default"] = ResourcePackage(with: Bundle.main.path(forResource: "themes", ofType: "pkg")!, encrypter: _encrypt, compressor: _compress)
        // NOTE: this "default" doesn't mean the "default" theme, it means default themes package file
        
        // loading patchs
        do {
            let pkgFolder = NSHomeDirectory() + "/Documents/levelPackages/"
            log.debug("加载额外关卡包: [\(pkgFolder)]")
            let files = try filemanager.contentsOfDirectory(atPath: pkgFolder)
            let packages = files.filter{$0.contains("pkg")}
            log.debug("  发现\(packages.count)个关卡包")
            for pkg in packages {
                levelResource.packages[pkg] = ResourcePackage(with: pkgFolder + pkg, encrypter: _encrypt, compressor: _compress)
            }
        } catch {
            log.error("ERROR: 额外关卡包加载失败")
        }
        
        do {
            let pkgFolder = NSHomeDirectory() + "/Documents/themePackages/"
            log.debug("加载额外主题包: [\(pkgFolder)]")
            let files = try filemanager.contentsOfDirectory(atPath: pkgFolder)
            let packages = files.filter{$0.contains("pkg")}
            log.debug("  发现\(packages.count)个主题包")
            for pkg in packages {
                themeResource.packages[pkg] = ResourcePackage(with: pkgFolder + pkg, encrypter: _encrypt, compressor: _compress)
            }
        } catch {
            log.error("ERROR: 额外主题包加载失败")
        }
        
        themeResource.theme = Config.default.theme
        themeResource.language = Config.default.language
        levelResource.language = Config.default.language
        Hero.levelPackageReader = levelResource
        Hero.heroStatus = Progress.default["hero"]
        Team.levelPackageReader = levelResource
        Team.teamStatus = Progress.default["team"]
        Action.levelPackageReader = levelResource
        ActionFactory.levelPackageReader = levelResource
    }
}

