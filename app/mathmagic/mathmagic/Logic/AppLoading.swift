//
//  AppLoading.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/4.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import UIKit
import KeyValueData
import DeviceKit

/// 加载进度更新代理协议
protocol AppLoadingProgressUpdateDelegate {
    func AppLoadingProgressUpdated()
}

/// App 加载进度管理
class AppLoading: NSObject {
    static var `default` = AppLoading()
    
    /// 任务队列
    let queue = DispatchQueue(label: "AppLoading", qos: .userInitiated)
    
    /// 需要完成的任务量 (不是任务数)
    private(set) var workload = 1 {
        didSet{
            progressUpdatedDelegate?.AppLoadingProgressUpdated()
        }
    }
    /// 已完成工作量
    private(set) var workfinished = 0 {
        didSet{
            progressUpdatedDelegate?.AppLoadingProgressUpdated()
        }
    }
    
    /// 当前执行任务名称
    var taskTitle = "加载启动任务" {
        didSet{
            progressUpdatedDelegate?.AppLoadingProgressUpdated()
        }
    }
    /// 当前任务进度
    var taskProgress: CGFloat = 0 {
        didSet{
            if taskProgress < 0 {
                taskProgress = 0
            }
            if taskProgress > 1 {
                taskProgress = 1
            }
            progressUpdatedDelegate?.AppLoadingProgressUpdated()
        }
    }
    
    /// 任务量数据更新代理
    var progressUpdatedDelegate: AppLoadingProgressUpdateDelegate?
    
    /// 工作完成进度
    var progress: CGFloat {
        get {
            return CGFloat(workfinished) / CGFloat(workload)
        }
    }
    
    /// 添加任务到队列
    func queueTask(title: String, taskworkload: Int, task: @escaping () -> Void) {
        log.debug("任务[\(title)]已加入启动任务队列")
        workload += taskworkload
        queue.async {
            log.verbose("  任务[\(title)]开始执行")
            self.taskTitle = title
            self.taskProgress = 0
            task()
            self.workfinished += taskworkload
            log.verbose("  任务[\(title)]执行完成")
        }
    }
    
    /// 构造函数自动添加内置加载模块
    private override init() {
        super.init()
        for (title, taskworkload, task) in _initTaskList {
            queueTask(title: title, taskworkload: taskworkload, task: task)
        }
        workfinished += 1
    }
    
    // MARK: - 在此添加启动项
    // 非内置加载项应集中在 AppDelegate 和 AppLoadingViewController 中加载
    
    /// 内置加载模块列表 (title, taskworkload, task)
    private let _initTaskList: [(String, Int, () -> Void)] =
        [
            ("系统加载中...", 1, AppLoading.waitAppLoadingViewControllerLoading),
            ("API权限申请中...", 1, AppLoading.initApiSystem),
            ("系统安装检测...", 5, AppLoading.checkAppInstallationLog),
            ]
    
    /// 启动任务样例
    private class func waitAppLoadingViewControllerLoading() {
        // 等待系统加载
        sleep(2)
        AppLoading.default.taskProgress = 1
        // anything
    }
    
    /// 标记系统安装时间
    private class func checkAppInstallationLog() {
        var data: KeyValueData = KeyValueDictionaryInKeychain(withKey: "apps", accessGroup: DeveloperIDPrefix + ".org.alfredg.games")
        var date: KeyValueData = KeyValueDictionaryInKeychain(withKey: "app_install_date", accessGroup: DeveloperIDPrefix + ".org.alfredg.games")

        var ident = "org.alfredg.mathmagic"
        if let identifier = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String {
            ident = identifier
        }
        var appversion = ""
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appversion = version
        }
        
        if data[ident] == nil {
            data[ident] = appversion
        }
        
        if date[ident + "." + appversion] == nil {
            date[ident + "." + appversion] = Date()
        }
    }
    
    /// 初始化 api 访问
    private class func initApiSystem() {
        _ = API.default
    }
}
