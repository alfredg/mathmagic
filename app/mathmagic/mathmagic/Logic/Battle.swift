//
//  Battle.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/19.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import UIKit

/// 战斗
class Battle: NSObject {
    /// 战斗双方
    /// team1 总是玩家一方
    let team1 : Team
    let team2 : Team
    
    /// 战场环境参数
    let envKey : String
    
    /// 动作队列
    var actionQueue : [Action] = []
    
    /// 战斗开始时间
    private var _startTime = Date()
    /// 开始战斗计时
    func startTimer() {
        _startTime = Date()
    }
    /// 战斗已消耗时间
    var time: TimeInterval {
        return Date().timeIntervalSince(_startTime)
    }
    
    
    
    init(_ t1 : Team, fightWith t2 : Team, at env: String = "") {
        team1 = t1
        team2 = t2
        envKey = env
    }
    
    /// 直接攻击伤害计算
    /// - parameter byPlayer: 攻击方向，true = 'team1 攻击 team2'
    /// - parameter envRandMax: 场景随机数，用于命中效果计算
    ///     - 取值范围: [0, 100]
    ///     - 伤害 = 基准伤害 * 命中 + 基准伤害 * (1 - 命中) * 场景随机数[0~1] - 防御
    /// - parameter attMultiple: 题目难度，用于攻击力计算
    ///     - 取值范围: [0, 200]
    ///     - 伤害 = (角色 ATT) * 题目 ATT%
    /// - returns: 伤害数值
    func attackDemage(byPlayer byTeam1: Bool = true, envRandMax : Int = 100, attMultiple : Int = 100) -> Int {
        var att : Int
        var def : Int
        var hit : Int
        
        if byTeam1 {
            // 伤害 = (角色 ATT) * 题目 ATT%
            att = team1.att * attMultiple / 100
            // 防御 = 角色 DEF
            def = team2.defense
            // 命中% = (命中 - 目标偏折)/(命中 + 目标偏折)
            hit = (team1.hit - team2.deflect) * 100 / (team1.hit + team2.deflect)
        } else {
            att = team2.att * attMultiple / 100
            def = team1.defense
            hit = (team2.hit - team1.deflect) * 100 / (team1.hit + team2.deflect)
        }
        // 0 <= 命中 <= 1 (0 <= hit <= 100)
        if hit < 0 {
            hit = 0
        }
        
        // 场景随机数
        // 伤害 = 基准伤害 * 命中 + 基准伤害 * (1 - 命中) * 场景随机数[0~1] - 防御
        var rand = Int(arc4random_uniform(UInt32(envRandMax)))
        if rand > 100 {
            rand = 100
        }
        
        // 伤害(不小于0) = 基准伤害 * 命中 + 基准伤害 * (1 - 命中) * 场景随机数[0~1] - 防御
        var demage = (att * hit + att * (100 - hit) * rand / 100) / 100 - def
        if demage < 0 {
            demage = 0
        }
        
        return demage
    }
}
