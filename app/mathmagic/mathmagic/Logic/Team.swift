//
//  Team.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/19.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import Foundation
import ResourcePackage
import KeyValueData

/// 战斗团队基本数据
class Team: NSObject {
    
    /// 关卡包读取器
    /// 预设团队相关基础数据与素材在 关卡包characters/teams/id 目录
    static var levelPackageReader = ResourcePackageReader()
    
    /// 进度数据读取器
    /// 英雄团队相关数据
    static var teamStatus : KeyValueData = KeyValueDictionaryInDocumentsPlist(withKey: "Team")
    
    /// 团队名称
    private(set) var name : String = ""
    
    /// 团队主英雄
    let mainHero : Hero
    /// 助手
    let assistent1 : Hero
    let assistent2 : Hero
    
    /// 战斗数据
    var hp : Int = 0
    var mp : Int = 0
    
    init(_ _hero: Hero, a1: Hero = Hero("zero"), a2: Hero = Hero("zero")) {
        mainHero = _hero
        assistent1 = a1
        assistent2 = a2
        
        super.init()
        hp = maxHP
        mp = maxMP
        name = mainHero.name
    }
    
    /// 获取首要团队
    convenience override init() {
        let mainHero = Hero("priencess")
        var a1 = Hero("zero")
        var a2 = Hero("zero")
        if Team.teamStatus["assistent1"] != nil {
            a1 = Hero(Team.teamStatus["assistent1"] as! String)
        }
        if Team.teamStatus["assistent2"] != nil {
            a2 = Hero(Team.teamStatus["assistent2"] as! String)
        }
        
        self.init(mainHero, a1: a1, a2: a2)
    }
    
    /// 获取预构建团队
    convenience init(team key: String) {
        var mainHero = Hero("rabbit")
        var a1 = Hero("zero")
        var a2 = Hero("zero")
        
        if let _data = Team.levelPackageReader["characters/teams/"+key+"/mainhero.key"],
            let _string = String(data: _data, encoding: .utf8) {
            mainHero = Hero(_string)
        }
        if let _data = Team.levelPackageReader["characters/teams/"+key+"/assistent1.key"],
            let _string = String(data: _data, encoding: .utf8) {
            a1 = Hero(_string)
        }
        if let _data = Team.levelPackageReader["characters/teams/"+key+"/assistent2.key"],
            let _string = String(data: _data, encoding: .utf8) {
            a2 = Hero(_string)
        }
        
        log.info("构建战斗团队: \(key) - [\(mainHero.name), \(a1.name), \(a2.name)]")
        self.init(mainHero, a1: a1, a2: a2)
        
        if let _data = Team.levelPackageReader["characters/teams/"+key+"/name.text"],
            let _string = String(data: _data, encoding: .utf8) {
            name = _string
        } else {
            name = key
        }
        
    }
    
    /// 级别
    var level : Int {
        return mainHero.level
    }
    
    var basehp : Int {
        return mainHero.hp + assistent1.hp + assistent2.hp
    }
    
    var hpExt : Int {
        return mainHero.hpExt + assistent1.hpExt + assistent2.hpExt
    }
    
    var maxHP : Int {
        return basehp * hpExt
    }
    
    var maxMP : Int {
        return mainHero.mp + assistent1.mp + assistent2.mp
    }
    
    var baseMpCost : Int {
        return mainHero.mp / 100
    }
    
    /// ATT : 一般攻击
    var att : Int {
        return mainHero.att + assistent1.att + assistent2.att
    }
    /// matt : (英雄)魔法攻击加成
    var matt : Int {
        return mainHero.matt + assistent1.matt + assistent2.matt
    }
    /// DEF : 防御 (直接减免伤害)
    var defense : Int {
        return mainHero.defense + assistent1.defense + assistent2.defense
    }
    /// hit : 命中
    var hit : Int {
        return mainHero.hit + assistent1.hit + assistent2.hit
    }
    /// deflect : 偏转
    var deflect : Int {
        return mainHero.deflect + assistent1.deflect + assistent2.deflect
    }

}
