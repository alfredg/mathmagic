//
//  Action.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/21.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import Foundation
import ResourcePackage

/// 战斗动作
///
/// **Action 不带有动态属性, action 升级用不同的 ID (即不同子类) 来体现**
class Action : NSObject {
    /// 关卡包读取器
    /// 动作基础数据与素材在 关卡包 actions/id 目录
    static var levelPackageReader = ResourcePackageReader()
    
    // 基础数值
    /// 动作ID
    let ID : String
    /// 动作名称
    let name : String
    /// 动作说明字符串
    /// 字符串中 $att 将被替换为 att
    let text : String
    /// 动作图标 (在 levelPackage 中的 key)
    let icon : String
    /// 动画 (在 levelPackage 中的 key)
    let animation : String
    /// 动作方向
    let byPlayer : Bool
    /// 时间片 (单位毫秒)
    var time : Int
    /// 目标 team
    let target : Team
    /// 基准数值
    let baseAtt : Int

    /// 增益效果队列

    
    init(_ key : String = "noaction", to _target : Team, byPlayer _byPlayer : Bool = true) {
        // 设置基本数据
        ID = key
        target = _target
        byPlayer = _byPlayer
        if let _data = Action.levelPackageReader["actions/"+key+"/name.text"],
            let _string = String(data: _data, encoding: .utf8) {
            name = _string
        } else {
            name = "No Action"
        }
        if let _data = Action.levelPackageReader["actions/"+key+"/text.text"],
            let _string = String(data: _data, encoding: .utf8) {
            text = _string
        } else {
            text = "Action of Nothing"
        }
        if let _data = Action.levelPackageReader["actions/"+key+"/icon.key"],
            let _string = String(data: _data, encoding: .utf8) {
            icon = _string
        } else {
            icon = ""
        }
        if let _data = Action.levelPackageReader["actions/"+key+"/animation.key"],
            let _string = String(data: _data, encoding: .utf8) {
            animation = _string
        } else {
            animation = ""
        }
        if let _data = Action.levelPackageReader["actions/"+key+"/time.number"],
            let _string = String(data: _data, encoding: .utf8),
            let _value = Int(_string) {
            time = _value
        } else {
            time = 1000
        }
        if let _data = Action.levelPackageReader["actions/"+key+"/att.number"],
            let _string = String(data: _data, encoding: .utf8),
            let _value = Int(_string) {
            baseAtt = _value
        } else {
            baseAtt = 0
        }
    }
    
    // 计算值
    /// 实际数值输出
    var att : Int {
        return baseAtt
    }

}

/// 动作工厂
class ActionFactory: NSObject {
    /// 关卡包读取器
    /// 战斗动作基础数据与素材在 关卡包 actions/id 目录
    static var levelPackageReader = ResourcePackageReader()
    
    private override init() {
    
    }
    
    /// 工厂方法, 制造相关 Action 实例
    static func createAction(_ actionID: String, to _target : Team, byPlayer _byPlayer : Bool = true) -> Action {
        switch actionID.lowercased() {
        case "normalhit":
            let _action = ActionNormalHit(to: _target, byPlayer: _byPlayer)
            return _action
        default:
            let _action = ActionNormalHit(to: _target, byPlayer: _byPlayer)
            return _action
        }
    }
}
