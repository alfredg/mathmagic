//
//  Hero.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/18.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import Foundation
import ResourcePackage
import KeyValueData

/// 英雄角色基本信息与动作
class Hero: NSObject {
    
    /// 关卡包读取器
    /// 英雄相关基础数据与素材在 关卡包characters/heros/id 目录
    static var levelPackageReader = ResourcePackageReader()
    
    /// 英雄进度与成长数据读取器
    /// 英雄相关数据存储在 reader[id.key]
    static var heroStatus : KeyValueData = KeyValueDictionaryInDocumentsPlist(withKey: "Hero")
    
    /// 英雄ID
    let ID : String
    
    // 英雄基础数据，init 从关卡包读取
    /// 基础智力
    let baseIntelligence : Int
    /// 基础体力
    let baseStrength : Int
    /// 基础洞察
    let baseInsight : Int
    /// 基础领导力
    let baseLeadship : Int
    /// 基础声望
    let baseReputation : Int
    /// 基础直觉
    let baseIntuition : Int
    /// 基础幸运
    let baseLucky : Int
    /// 基本图片
    let baseIconKey : String
    /// 显示名称
    let name : String
    
    init(_ key : String = "rabbit") {
        // 设置基本数据
        ID = key
        if let _data = Hero.levelPackageReader["characters/heros/"+key+"/intelligence.number"],
            let _string = String(data: _data, encoding: .utf8),
            let _value = Int(_string) {
            baseIntelligence = _value
        } else {
            baseIntelligence = 1
        }
        if let _data = Hero.levelPackageReader["characters/heros/"+key+"/strength.number"],
            let _string = String(data: _data, encoding: .utf8),
            let _value = Int(_string) {
            baseStrength = _value
        } else {
            baseStrength = 1
        }
        if let _data = Hero.levelPackageReader["characters/heros/"+key+"/insight.number"],
            let _string = String(data: _data, encoding: .utf8),
            let _value = Int(_string) {
            baseInsight = _value
        } else {
            baseInsight = 0
        }
        if let _data = Hero.levelPackageReader["characters/heros/"+key+"/leadship.number"],
            let _string = String(data: _data, encoding: .utf8),
            let _value = Int(_string) {
            baseLeadship = _value
        } else {
            baseLeadship = 0
        }
        if let _data = Hero.levelPackageReader["characters/heros/"+key+"/reputation.number"],
            let _string = String(data: _data, encoding: .utf8),
            let _value = Int(_string) {
            baseReputation = _value
        } else {
            baseReputation = 0
        }
        if let _data = Hero.levelPackageReader["characters/heros/"+key+"/intuition.number"],
            let _string = String(data: _data, encoding: .utf8),
            let _value = Int(_string) {
            baseIntuition = _value
        } else {
            baseIntuition = 1
        }
        if let _data = Hero.levelPackageReader["characters/heros/"+key+"/lucky.number"],
            let _string = String(data: _data, encoding: .utf8),
            let _value = Int(_string) {
            baseLucky = _value
        } else {
            baseLucky = 10000
        }
        if let _data = Hero.levelPackageReader["characters/heros/"+key+"/icon.key"],
            let _string = String(data: _data, encoding: .utf8) {
            baseIconKey = _string
        } else {
            baseIconKey = ""
        }
        if let _data = Hero.levelPackageReader["characters/heros/"+key+"/name.text"],
            let _string = String(data: _data, encoding: .utf8) {
            name = _string
        } else {
            name = "未知生物"
        }
        super.init()
        
        // 生成英雄进度数据
        if Hero.heroStatus[ID+".nosave"] as? Bool ?? true {
            // 是否在 progress 中记录英雄进度
            if let _data = Hero.levelPackageReader["characters/heros/"+key+"/nosave.bool"],
                let _string = String(data: _data, encoding: .utf8),
                let _value = Bool(_string) {
                Hero.heroStatus[ID+".nosave"] = _value
            } else {
                Hero.heroStatus[ID+".nosave"] = false
            }
            
            // 起始级别
            if let _data = Hero.levelPackageReader["characters/heros/"+key+"/startlevel.number"],
                let _string = String(data: _data, encoding: .utf8),
                let _value = Int(_string) {
                Hero.heroStatus[ID+".level"] = _value
            } else {
                Hero.heroStatus[ID+".level"] = 1
            }
            
            // 起始经验
            if let _data = Hero.levelPackageReader["characters/heros/"+key+"/startexp.number"],
                let _string = String(data: _data, encoding: .utf8),
                let _value = Int(_string) {
                Hero.heroStatus[ID+".exp"] = _value
            } else {
                Hero.heroStatus[ID+".exp"] = 0
            }
            
        }
    }
    
    // 英雄进度数据，与数据存储同步
    /// 级别
    private(set) var level : Int {
        get {
            return Hero.heroStatus[ID+".level"] as! Int
        }
        set {
            Hero.heroStatus[ID+".level"] = newValue
        }
    }
    /// 经验值
    private(set) var exp : Int {
        get {
            return Hero.heroStatus[ID+".exp"] as! Int
        }
        set {
            Hero.heroStatus[ID+".exp"] = newValue
        }
    }
    /// 级别经验值计算函数
    ///
    /// 级别经验 = 100 * Lv * (Lv + 1) * (Lv + 2)
    /// - parameter level: 级别
    /// - returns: 级别需要的全部经验值
    func levelExp(_ level: Int) -> Int {
        if level == 0 {
            return 0
        }
        return 100 * level * (level + 1) * (level + 2)
    }
    /// 英雄当前级别升级经验值
    var expUp : Int {
        return levelExp(level)
    }
    /// 英雄获得经验
    /// - parameter _exp: 获得的经验值
    /// - returns: 是否导致升级
    func addExp(_ _exp: Int) -> Bool {
        var newexp = exp + _exp
        var lvup = false
        while newexp >= expUp {
            newexp -= expUp
            level += 1
            lvup = true
        }
        exp = newexp
        return lvup
    }
    
    // 英雄参数，实时计算产生
    /// 智力 = 120 + Lv * 2 * 基础智力
    var intelligence : Int {
        if level == 0 {
            return 0
        }
        return 120 + level * 2 * baseIntelligence
    }
    /// 体力 = 60 + Lv * 基础体力
    var strength : Int {
        if level == 0 {
            return 0
        }
        return 60 + level * baseStrength
    }
    /// 洞察 = 基础洞察力 * Lv
    var insight : Int {
        return level * baseInsight
    }
    /// 领导力 = 基础领导力 * Lv * Lv
    var leadship : Int {
        return level * level * baseLeadship
    }
    /// 声望 = 基础声望 * Lv * Lv
    var reputation : Int {
        return level * level * baseReputation
    }
    /// 直觉
    var intuition : Int {
        if level == 0 {
            return 0
        }
        return baseIntuition
    }
    /// 幸运
    var lucky : Int {
        if level == 0 {
            return 0
        }
        return baseLucky
    }
    /// 图片
    var icon : UIImage? {
        log.debug("[HERO: \(self.ID)]: 获取\(self.name)图片: \(self.baseIconKey)")
        return Hero.levelPackageReader.getImage(baseIconKey)
    }
    
    // 战斗力参数，实时计算产生
    /// HP
    /// HP = 2000 * Lv
    var hp : Int {
        return 2000 * level
    }
    /// MP
    /// MP = 100 * Lv * 耐力
    var mp : Int {
        return 100 * level * strength
    }
    /// HP+ : HP 倍率加成 **注意这个是100倍大的**
    /// HP+ = (智力 + 领导力) / 500
    var hpExt : Int {
        return (intelligence + leadship) / 5
    }
    /// ATT : 一般攻击
    /// ATT = 领导力 + 声望
    var att : Int {
        return leadship + reputation
    }
    /// matt : (英雄)魔法攻击加成 **注意这个是100倍大的**
    /// 魔法ATT% = 智力/100 - 1
    var matt : Int {
        if level == 0 {
            return 0
        }
        return intelligence - 100
    }
    /// DEF : 防御 (直接减免伤害)
    /// DEF = 直觉
    var defense : Int {
        return intuition
    }
    /// hit : 命中
    /// 命中 = 洞察力 * 直觉 + 领导力 * 幸运
    var hit : Int {
        return insight * intuition + leadship * lucky
    }
    /// deflect : 偏转
    /// 偏折 = 洞察力 * 直觉 + 声望 * 幸运
    var deflect : Int {
        return insight * intuition + reputation * lucky
    }
    
}
