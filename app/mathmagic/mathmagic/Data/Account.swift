//
//  Account.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/14.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import UIKit
import KeyValueData

/// 用户帐号
class Account: NSObject {

    static let `default` = Account()
    override private init() {
        super.init()
        
        if _data["UUID"] == nil {
            _data["UUID"] = UUID()
        }
        
        log.info("帐号数据加载, UUID: \(self._data["UUID"] as! UUID), 配置数据\(self._data.count)项")
    }
    
    /// 数据存储器
    private var _data: KeyValueData = KeyValueDictionaryInKeychain(withKey: "account_mathmagic")
    
    // 帐号数据信息
    func set(key: String, value: Any, onFail: @escaping () -> () = {}) {
        self._data[key] = value
    }
    
    func get(key: String, queryServer: Bool = true, callback: @escaping (Any?) -> () = {_ in }) -> Any? {
        return _data[key]
    }
    
    // !!!测试用!!! 帐号数据输出
    func _dump2log() {
        log.info("========================================")
        log.info("========================================")
        log.info("开始帐号数据输出, 共\(self._data.count)数据项:")
        for key in _data.keys {
            log.info("  [\(key)] -> [\(String(describing: self._data[key]))]")
        }
        log.info("========================================")
    }
    
    /// 设备唯一标识
    var uuid: UUID {
        return _data["UUID"] as! UUID
    }
    
    
}
