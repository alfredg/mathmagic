//
//  Progress.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/14.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import UIKit
import KeyValueData

/// 进度管理
class Progress: NSObject {
    
    /// 本地用户帐号对象单例
    static let `default` = Progress()
    override private init() {
        super.init()
        
        for key in propretyList {
            data[key] = KeyValueDictionaryInSqlite(withKey: key, inDatabase: "progress")
        }
        
        log.info("进度数据加载")
        
    }
    
    /// 数据存储
    private let propretyList = ["general",  // 一般数据
                                "hero",     // 英雄相关数据
                                "team",     // 团队相关数据
                                ]
    private var data: [String : KeyValueData] = [:]
    
    /// 数据访问接口
    subscript(proprety: String) -> KeyValueData {
        #if DEBUG
            return data[proprety]!
        #else
            return data[proprety] ?? data["general"]!
        #endif
    }
    
    // !!!测试用!!! 进度数据输出
    func _dump2log() {
        log.info("========================================")
        log.info("开始进度数据输出")
        for prop in propretyList {
            log.info("----------------------------------------")
            log.info("开始进度数据[\(prop)]输出, 共\(self.data[prop]!.count)数据项:")
            for key in data[prop]!.keys {
                log.info("  [\(key)] -> [\(String(describing: self.data[prop]![key]))]")
            }
        }
        log.info("========================================")
    }
    
    // MARK: 常用数据
    
    /// 金币总数
    var coin : Int {
        get {
            return (data["general"]!["coin"] ?? 0) as! Int
        }
        set {
            data["general"]!["coin"] = newValue
        }
    }

}
