//
//  IAP.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/3/17.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import SwiftyJSON
import StoreKit

/// 内购数据
class IAP: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    static var `default` = IAP()
    
    static let IAPPurchasedNotification = "IAPPurchasedNotification"
    static let IAPFailedNotification = "IAPFailedNotification"
    static let IAPProductFeatchedNotification = "IAPProductFeatchedNotification"
    
    /// iap 产品列表
    var products : [SKProduct]?
    var productIDs : [String] = []
    
    private override init() {
        super.init()
        let app = UIApplication.shared.delegate as! AppDelegate
        //let bundleID = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String
        let iapConfig = JSON(data: app.levelResource["iap/iap.json"]!)
        for json in iapConfig["productlist"] {
            productIDs.append(json.1.stringValue)
        }
        
        log.debug("IAP: 产品列表 \(self.productIDs)")
        SKPaymentQueue.default().add(self)
    }
    
    /// 获取产品表
    func fetchProducts() {
        let request = SKProductsRequest(productIdentifiers: Set(self.productIDs))
        request.delegate = self
        request.start()
    }
    
    /// 恢复购买
    func restore() {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    // MARK: SKPaymentTransactionObserver
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        self.products = response.products
        
        log.info("IAP: product info featch, got \(response.products.count)")
        log.info("IAP: unavailible products: \(response.invalidProductIdentifiers)")
        for product in response.products {
            log.debug("IAP: [\(product.productIdentifier)] [\(product.localizedTitle) : \(product.localizedDescription)] -> [\(product.priceFormatted ?? "")]")
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: IAP.IAPProductFeatchedNotification), object: nil)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        log.debug("IAP: Updating IAP Status")
        
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchasing:
                log.debug("  IAP: ourchasing: [\(String(describing: transaction.transactionIdentifier))]:[\(transaction.payment.productIdentifier)]")
                break
            case .purchased:
                self.purchasedTransaction(transaction: transaction)
                log.debug("  IAP: purchased: [\(String(describing: transaction.transactionIdentifier))]:[\(transaction.payment.productIdentifier)]")
                break
            case .failed:
                self.failedTransaction(transaction: transaction)
                log.debug("  IAP: failed: [\(String(describing: transaction.transactionIdentifier))]:[\(transaction.payment.productIdentifier)] - [\(String(describing: transaction.error))]")
                break
            case .restored:
                self.restoreTransaction(transaction: transaction)
                log.debug("  IAP: restored: [\(String(describing: transaction.transactionIdentifier))]:[\(transaction.payment.productIdentifier)]")
                break
            default:
                log.debug("  IAP: [\(String(describing: transaction.transactionIdentifier)) : \(transaction.transactionState)] [\(String(describing: transaction.error))]")
                break
            }
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        log.error("IAP: restore failed [\(error)]")
        NotificationCenter.default.post(name: Notification.Name(rawValue: IAP.IAPFailedNotification), object: nil, userInfo: ["error" : error.localizedDescription])
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        log.error("IAP: restore succeed")
        //NotificationCenter.default.post(name: Notification.Name(rawValue: IAP.IAPPurchasedNotification), object: nil)
    }
    
    // MARK: Transaction
    
    func finishTransaction(transaction: SKPaymentTransaction? = nil) {
        if let transaction = transaction {
            SKPaymentQueue.default().finishTransaction(transaction)
        }
    }
    
    func restoreTransaction(transaction: SKPaymentTransaction? = nil) {
        log.debug("IAP: transaction restored: [\(String(describing: transaction))]")
        if let pkg = transaction?.payment.productIdentifier {
            log.debug("  IAP: \(pkg) restored")
        }
        self.finishTransaction(transaction: transaction)
        NotificationCenter.default.post(name: Notification.Name(rawValue: IAP.IAPPurchasedNotification), object: nil)
    }
    
    func failedTransaction(transaction: SKPaymentTransaction? = nil, error: Error? = nil) {
        log.debug("IAP: trans failed: [\(String(describing: transaction)) : \(String(describing: error))]")
        self.finishTransaction(transaction: transaction)
        NotificationCenter.default.post(name: Notification.Name(rawValue: IAP.IAPFailedNotification), object: nil, userInfo: ["error" : transaction?.error?.localizedDescription ?? "未知故障"])
    }
    
    func purchasedTransaction(transaction: SKPaymentTransaction? = nil) {
        log.debug("IAP: trans succeed: [\(String(describing: transaction))]")

        if let pkg = transaction?.payment.productIdentifier {
            log.debug("  IAP: \(pkg) purchased")
            var receiptString = ""
            if let receiptUrl = Bundle.main.appStoreReceiptURL, let receiptData = NSData(contentsOf: receiptUrl) {
                receiptString = receiptData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                // 收据本地验证
            }
            log.debug("  IAP receipt: [\(receiptString)]")
            // 服务器记录
            API.default.iapCheck(product: pkg, receipt: receiptString)
            // 不验证直接发货
            
        }
        self.finishTransaction(transaction: transaction)
        NotificationCenter.default.post(name: Notification.Name(rawValue: IAP.IAPPurchasedNotification), object: nil)
    }
}

/// 产品扩展
extension SKProduct {
    
    private var payment: SKPayment {
        get { return SKPayment(product: self) }
    }
    
    public var priceFormatted: String? {
        get {
            let priceFormatter = NumberFormatter()
            priceFormatter.formatterBehavior = NumberFormatter.Behavior.behavior10_4
            priceFormatter.numberStyle = NumberFormatter.Style.currency
            priceFormatter.locale = self.priceLocale
            return priceFormatter.string(from: self.price)!
        }
    }
    
    public func buy() {
        if SKPaymentQueue.canMakePayments() {
            SKPaymentQueue.default().add(self.payment)
        }
    }
    
}

