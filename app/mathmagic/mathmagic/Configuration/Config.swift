//
//  Config.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/4.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import UIKit
import KeyValueData

/// 应用设置
class Config: NSObject {
    /// 应用配置对象单例
    static let `default` =  Config()
    override private init() {
        super.init()
        
        log.info("配置数据已读取, 加载\(self._data.count)配置项")
    }
    
    /// 数据存储器
    private var _data: KeyValueData = KeyValueDictionaryInUserDefaults(withKey: "AppConfig")
    
    // !!!测试用!!! 帐号数据输出
    func _dump2log() {
        log.info("========================================")
        log.info("========================================")
        log.info("开始配置数据输出, 共\(self._data.count)数据项:")
        for key in _data.keys {
            log.info("  [\(key)] -> [\(String(describing: self._data[key]))]")
        }
        log.info("========================================")
    }
    
    /// 清除数据
    func clean() {
        _data.clean()
    }
    
    /// 强制存储
    func save() {
        log.verbose("配置信息存储中, 共\(self._data.count)配置项")
        _data.sync()
    }
    
    // MARK: -配置信息
    /// 开启测试功能
    var isTestEnabled: Bool {
        get {
            if let _value = _data["isTestEnabled"] {
                return _value as! Bool
            } else {
                return false
            }
        }
        set {
            _data["isTestEnabled"] = newValue
            
            log.info("设置应用测试模式: \(newValue)")
        }
    }
    
    /// 主题样式
    var theme: String {
        get {
            if let _value = _data["theme"] {
                return _value as! String
            } else {
                return "default"
            }
        }
        set {
            _data["theme"] = newValue
            
            log.info("设置主题: \(newValue)")
        }
    }
    
    /// 静音
    var isMuteEnabled: Bool {
        get {
            if let _value = _data["isMuteEnabled"] {
                return _value as! Bool
            } else {
                #if DEBUG
                    return true
                #else
                    return false
                #endif
            }
        }
        set {
            _data["isMuteEnabled"] = newValue
            
            log.info("设置静音模式: \(newValue)")
        }
    }
    /// 音量
    var soundVolume: Float {
        get {
            if let _value = _data["soundVolume"] {
                return _value as! Float
            } else {
                return 0.75
            }
        }
        set {
            _data["soundVolume"] = newValue
            
            log.info("设置音量: \(newValue)")
        }
    }
    
    /// 背景音量
    var bgmVolume: Float {
        get {
            if let _value = _data["bgmVolume"] {
                return _value as! Float
            } else {
                return 0.5
            }
        }
        set {
            _data["bgmVolume"] = newValue
            
            log.info("设置背景音量: \(newValue)")
        }
    }
    
    /// 语言
    var  language: String {
        get {
            if let _value = _data["language"] {
                return _value as! String
            } else {
                return NSLocalizedString("ThemeLanguageSufix", comment: "界面资源包语言后缀")
            }
        }
        set {
            _data["language"] = newValue
            
            log.info("设置界面语言: \(newValue)")
        }
    }

}
