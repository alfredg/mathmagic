//
//  Created by  fred on 2016/10/26.
//  Copyright © 2016年 Alibaba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CloudApiSdk.h"

@interface ApiClient_alapi : NSObject
+ (instancetype) instance;

- (void) updateDeviceInfo:(NSString *) token name:(NSString *) name devicetype:(NSString *) devicetype osversion:(NSString *) osversion app:(NSString *) app appversion:(NSString *) appversion comments:(NSString *) comments completionBlock:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock;
- (void) iaplog_ios:(NSString *) token app:(NSString *) app product:(NSString *) product receipt:(NSString *) receipt completionBlock:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock;
- (void) register:(NSString *) id pwd:(NSString *) pwd type:(NSString *) type completionBlock:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock;    
- (void) login:(NSString *) id pwd:(NSString *) pwd completionBlock:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock;
- (void) ping:(NSString *) echo completionBlock:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock;
- (void) tokenInfo:(NSString *) token completionBlock:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock;
- (void) versionInfo:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock;
@end
