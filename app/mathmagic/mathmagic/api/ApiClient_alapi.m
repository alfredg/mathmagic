//
//  Created by  fred on 2016/10/26.
//  Copyright © 2016年 Alibaba. All rights reserved.
//

#import "ApiClient_alapi.h"
#import "HttpConstant.h"

@implementation ApiClient_alapi

static NSString* HOST = @"api.alfredg.org";

+ (instancetype)instance {
    static dispatch_once_t onceToken;
    static ApiClient_alapi *api = nil;
    dispatch_once(&onceToken, ^{
        api = [ApiClient_alapi new];
    });
    return api;
}

- (instancetype)init {
    self = [super init];
    return self;
}

    - (void) updateDeviceInfo:(NSString *) token name:(NSString *) name devicetype:(NSString *) devicetype osversion:(NSString *) osversion app:(NSString *) app appversion:(NSString *) appversion comments:(NSString *) comments completionBlock:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock
    {
        
        //定义Path
        NSString * path = @"/reg/deviceinfo";
        
        NSMutableDictionary *queryParams = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *headerParams = [[NSMutableDictionary alloc] init];
        [headerParams setValue:token forKey:@"token"];
        [queryParams setValue:name forKey:@"name"];
        [queryParams setValue:devicetype forKey:@"devicetype"];
        [queryParams setValue:osversion forKey:@"osversion"];
        [queryParams setValue:app forKey:@"app"];
        [queryParams setValue:appversion forKey:@"appversion"];
        [queryParams setValue:comments forKey:@"comments"];
        
        [[CloudApiSdk instance] httpPost: CLOUDAPI_HTTPS
                                    host: HOST
                                    path: path
                              pathParams: nil
                             queryParams: queryParams
                              formParams: nil
                            headerParams: headerParams
                         completionBlock: completionBlock];
        
    }
    
    - (void) iaplog_ios:(NSString *) token app:(NSString *) app product:(NSString *) product receipt:(NSString *) receipt completionBlock:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock
    {
        
        //定义Path
        NSString * path = @"/iap/ioslog";
        
        NSMutableDictionary *queryParams = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *headerParams = [[NSMutableDictionary alloc] init];
        [headerParams setValue:token forKey:@"token"];
        [queryParams setValue:app forKey:@"app"];
        [queryParams setValue:product forKey:@"product"];
        [queryParams setValue:receipt forKey:@"receipt"];
        
        [[CloudApiSdk instance] httpPost: CLOUDAPI_HTTPS
                                    host: HOST
                                    path: path
                              pathParams: nil
                             queryParams: queryParams
                              formParams: nil
                            headerParams: headerParams
                         completionBlock: completionBlock];
        
    }
    
    - (void) register:(NSString *) id pwd:(NSString *) pwd type:(NSString *) type completionBlock:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock
    {
        
        //定义Path
        NSString * path = @"/reg";
        
        NSMutableDictionary *queryParams = [[NSMutableDictionary alloc] init];
        [queryParams setValue:id forKey:@"id"];
        [queryParams setValue:pwd forKey:@"pwd"];
        [queryParams setValue:type forKey:@"type"];
        
        [[CloudApiSdk instance] httpPost: CLOUDAPI_HTTPS
                                    host: HOST
                                    path: path
                              pathParams: nil
                             queryParams: queryParams
                              formParams: nil
                            headerParams: nil
                         completionBlock: completionBlock];
        
    }

    - (void) login:(NSString *) id pwd:(NSString *) pwd completionBlock:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock
    {

    //定义Path
    NSString * path = @"/token";

    NSMutableDictionary *queryParams = [[NSMutableDictionary alloc] init];
	[queryParams setValue:id forKey:@"id"];
	[queryParams setValue:pwd forKey:@"pwd"];

    [[CloudApiSdk instance] httpGet: CLOUDAPI_HTTPS
    host: HOST
    path: path
    pathParams: nil
	queryParams: queryParams
	headerParams: nil
    completionBlock: completionBlock];

    }

    - (void) ping:(NSString *) echo completionBlock:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock
    {

    //定义Path
    NSString * path = @"/ping";

    NSMutableDictionary *queryParams = [[NSMutableDictionary alloc] init];
	[queryParams setValue:echo forKey:@"echo"];

    [[CloudApiSdk instance] httpGet: CLOUDAPI_HTTPS
    host: HOST
    path: path
    pathParams: nil
	queryParams: queryParams
	headerParams: nil
    completionBlock: completionBlock];

    }

    - (void) tokenInfo:(NSString *) token completionBlock:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock
    {

    //定义Path
    NSString * path = @"/token/info";

    NSMutableDictionary *headerParams = [[NSMutableDictionary alloc] init];
	[headerParams setValue:token forKey:@"token"];

    [[CloudApiSdk instance] httpGet: CLOUDAPI_HTTPS
    host: HOST
    path: path
    pathParams: nil
	queryParams: nil
	headerParams: headerParams
    completionBlock: completionBlock];

    }

    - (void) versionInfo:(void (^)(NSData * , NSURLResponse * , NSError *))completionBlock
    {

    //定义Path
    NSString * path = @"/ver";

    

    [[CloudApiSdk instance] httpGet: CLOUDAPI_HTTPS
    host: HOST
    path: path
    pathParams: nil
	queryParams: nil
	headerParams: nil
    completionBlock: completionBlock];

    }


@end
