//
//  API.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/3/17.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import SwiftyJSON
import KeyValueData
import DeviceKit

/// 网络 api 访问
class API: NSObject {
    /// API对象单例
    static let `default` =  API()
    
    private var deviceid = ""
    private var devicepwd = ""
    private(set) var token = ""
    
    override private init() {
        super.init()
        AppConfiguration.instance().app_KEY = aliAppKey
        AppConfiguration.instance().app_SECRET = aliAppSecret
        
        let accountData = KeyValueDictionaryInKeychain(withKey: "account", accessGroup: DeveloperIDPrefix + ".org.alfredg.api")
        if accountData["deviceid"] == nil {
            registerDevice()
        } else {
            deviceid = accountData["deviceid"] as! String
            devicepwd = accountData["devicepwd"] as! String
            log.info("Device already registered: [\(self.deviceid) : \(self.devicepwd)]")
            updateDeviceInfo()
        }
    }
    
    private func registerDevice() {
        let accountData = KeyValueDictionaryInKeychain(withKey: "account", accessGroup: DeveloperIDPrefix + ".org.alfredg.api")
        if accountData["deviceid"] == nil {
            ApiClient_alapi().register(UUID().uuidString, pwd: "12345", type: "device") {
                (data, response, error) in
                log.verbose("register reponsed: [\(response.debugDescription)]")
                if data != nil {
                    let json = JSON(data: data!)
                    if json["success"].boolValue {
                        log.info("register succeed")
                        accountData["deviceid"] = json["id"].stringValue
                        accountData["devicepwd"] = "12345"
                        self.deviceid = accountData["deviceid"] as! String
                        self.devicepwd = accountData["devicepwd"] as! String
                        self.login {
                            self.updateDeviceInfo("Register")
                        }
                    } else {
                        log.error("register failed. RESP: [\(String(describing: response))] ERROR: [\(String(describing: error))]")
                    }
                }
            }
        }
    }
    
    private func login(_ onSuccess: @escaping () -> () = {}, onFail: @escaping () -> () = {}) {
        ApiClient_alapi().login(deviceid, pwd: devicepwd) {
            (data, response, error) in
            log.verbose("login reponsed: [\(response.debugDescription)]")
            
            if data != nil {
                let json = JSON(data: data!)
                if json["token"] != JSON.null {
                    self.token = json["token"].stringValue
                    if let identifier = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String {
                        let loginData = KeyValueDictionaryInKeychain(withKey: "lastlogin", accessGroup: DeveloperIDPrefix + ".org.alfredg.api")
                        loginData[identifier] = Date()
                    }
                    onSuccess()
                }
            } else {
                onFail()
            }
        }
    }
    
    private func updateDeviceInfo(_ comments: String = "") {
        let device = Device()
        var appname = "alfredg's app"
        if let name = Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String {
            appname = name
        }
        if let identifier = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String {
            appname += " [\(identifier)]"
        }
        var appversion = ""
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appversion = version
        }
        if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            appversion += " [Build:\(build)]"
        }
        
        ApiClient_alapi().updateDeviceInfo(token, name: device.name, devicetype: device.description, osversion: device.systemName + " " + device.systemVersion, app: appname, appversion: appversion, comments: comments) {
            (data, response, error) in
            if (response as! HTTPURLResponse).statusCode == 401 {
                self.login({
                    ApiClient_alapi().updateDeviceInfo(self.token, name: device.name, devicetype: device.description, osversion: device.systemName + " " + device.systemVersion, app: appname, appversion: appversion, comments: comments) {
                        (data, response, error) in
                        log.info("DeviceInfo Updated")
                    }
                }, onFail: {
                    log.error("Device login failed")
                })
            } else if (response as! HTTPURLResponse).statusCode == 200 {
                log.info("DeviceInfo Updated")
            } else {
                log.error("DeviceInfo Updated. RESP: [\(String(describing: response))] ERROR: [\(String(describing: error))]")
            }
        }
    }

    enum IAPError: String {
        case APILoginFailed
        case APIServerError
        case AppleServerError
        case SubscriptionExpired
        case IAPReceiptValidationFailed
    }
    func iapCheck(product: String, receipt: String, callbackKey key: String = "", onSuccess: @escaping (String, JSON) -> () = {_,_ in }, onFail: @escaping (String, IAPError) -> () = {_,_ in }) {
        login({
            var app = "org.alfredg.mathmagic"
            if let identifier = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String {
                app = identifier
            }
            ApiClient_alapi().iaplog_ios(self.token, app: app, product: product, receipt: receipt) {
                (data, response, error) in
                if data != nil {
                    let json = JSON(data: data!)
                    if let success = json["success"].bool {
                        if success {
                            onSuccess(key, json["receipt"])
                        } else {
                            // API 服务器返回失败信息
                            switch json["error"].stringValue {
                            case "Apple server connection error":
                                onFail(key, .AppleServerError)
                            case "Apple server error":
                                onFail(key, .AppleServerError)
                            case "Database error":
                                onFail(key, .APIServerError)
                            case "No data from Apple server":
                                onFail(key, .AppleServerError)
                            case "IAP Failed":
                                switch json["iapstatus"].intValue {
                                case 21006:
                                    onFail(key, .SubscriptionExpired)
                                case 21005:
                                    onFail(key, .AppleServerError)
                                default:
                                    onFail(key, .IAPReceiptValidationFailed)
                                }
                            default:
                                onFail(key, .APIServerError)
                            }
                            
                        }
                    } else {
                        // API 返回数据结构错误
                        onFail(key, .APIServerError)
                    }
                } else {
                    // API 服务器未返回数据
                    onFail(key, .APIServerError)
                }
            }
        }, onFail: {
            onFail(key, .APILoginFailed)
        })
    }
}
