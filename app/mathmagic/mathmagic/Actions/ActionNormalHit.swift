//
//  ActionNormalHit.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/23.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import UIKit

class ActionNormalHit: Action {
    init(to _target : Team, byPlayer _byPlayer : Bool = true) {
        super.init("normalhit", to: _target, byPlayer: _byPlayer)
    }
}
