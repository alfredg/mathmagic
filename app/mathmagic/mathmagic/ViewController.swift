//
//  ViewController.swift
//  mathmagic
//
//  Created by Alfred Gao on 2017/2/4.
//  Copyright © 2017年 Alfred Gao. All rights reserved.
//

import UIKit
import YLProgressBar

class ViewController: UIViewController, AppLoadingProgressUpdateDelegate {
    let app = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var imageLoading: UIImageView!
    @IBOutlet weak var imageLoaded: UIImageView!
    @IBOutlet weak var progressBar: YLProgressBar!
    @IBOutlet weak var progressText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        AppLoading.default.progressUpdatedDelegate = self
        
        background.setImage(from: app.themeResource, key: "loading/background.image")
        imageLoading.setImage(from: app.themeResource, key: "loading/loading.image")
        imageLoaded.setImage(from: app.themeResource, key: "loading/loaded.image")
        
        for i in 0..<15 {
            AppLoading.default.queueTask(title: "demo test \(i)", taskworkload: 1) {
                usleep(100000)
                print("task \(i) finished")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func AppLoadingProgressUpdated() {
        let _number = " \(Int(AppLoading.default.progress * 100))%"
        let _title = "\(AppLoading.default.taskTitle)"
        var _text = _title + _number
        if let _formatPrefix = app.themeResource.getString("loading/progresstextformat") {
            _text = _formatPrefix + _text
        }
        let barText = app.themeResource.themeTextFormater.format(_text)
        DispatchQueue.main.async {
            self.progressBar.progress = AppLoading.default.progress
            self.progressText.attributedText = barText
            self.imageLoaded.alpha = AppLoading.default.progress
        }
        
        if AppLoading.default.progress >= 1 {
            AppLoading.default.progressUpdatedDelegate = nil
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "finishloading", sender: nil)
            }
        }
    }
}

